package com.globant.converge.dtos.assessment;

import lombok.Data;

import java.util.List;

@Data
public class AssessmentDto {

    private long id;

    private List<QuestionDto> questions;
}
