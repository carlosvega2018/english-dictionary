package com.globant.converge.dtos.assessment;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrackDTO {

    private long id;

    private String name;

    private double grade;

}
