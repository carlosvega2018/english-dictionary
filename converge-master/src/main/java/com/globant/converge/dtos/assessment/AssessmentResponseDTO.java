package com.globant.converge.dtos.assessment;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class AssessmentResponseDTO {

    private double grade;

    private MaturityLevelDTO maturityLevelDTO;

    private List<TrackDTO> trackListDTO;
}
