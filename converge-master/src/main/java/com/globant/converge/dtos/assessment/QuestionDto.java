package com.globant.converge.dtos.assessment;

import lombok.Data;

import java.util.List;

@Data
public class QuestionDto {

    private long id;
    private String name;
    private List<AnswerDto> answers;
}
