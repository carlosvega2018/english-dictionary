package com.globant.converge.dtos.assessment;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Setter
@Getter
public class AssessmentRequestDTO {

    private List<AnswersCombined> answersCombined;

    private long assessmentId;

    private long attemptId;

}
