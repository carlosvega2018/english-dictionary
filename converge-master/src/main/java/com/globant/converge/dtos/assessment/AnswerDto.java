package com.globant.converge.dtos.assessment;

import lombok.Data;

@Data
public class AnswerDto {

    private long id;
    private String name;
}
