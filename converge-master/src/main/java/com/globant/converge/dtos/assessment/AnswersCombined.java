package com.globant.converge.dtos.assessment;

public class AnswersCombined {


    private long questionId;
    private long answerId;
    private int answerOrder;
    private long questionTrackId;
    private long questionAttributeId;


    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public int getAnswerOrder() {
        return answerOrder;
    }

    public void setAnswerOrder(int answerOrder) {
        this.answerOrder = answerOrder;
    }

    public long getQuestionTrackId() {
        return questionTrackId;
    }

    public void setQuestionTrackId(long questionTrackId) {
        this.questionTrackId = questionTrackId;
    }

    public long getQuestionAttributeId() {
        return questionAttributeId;
    }

    public void setQuestionAttributeId(long questionAttributeId) {
        this.questionAttributeId = questionAttributeId;
    }
}
