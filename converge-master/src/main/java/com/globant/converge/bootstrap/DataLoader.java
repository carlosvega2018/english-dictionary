package com.globant.converge.bootstrap;

import com.globant.converge.model.*;
import com.globant.converge.services.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    private final AssessmentService assessmentService;
    private final QuestionTypesService questionTypesService;
    private final QuestionService questionService;
    private final TrackService trackService;
    private final AttributeService attributeService;
    private final AnswerService answerService;

    public DataLoader(AssessmentService assessmentService, QuestionTypesService questionTypesService, QuestionService questionService, TrackService trackService, AttributeService attributeService, AnswerService answerService) {
        this.assessmentService = assessmentService;
        this.questionTypesService = questionTypesService;
        this.questionService = questionService;
        this.trackService = trackService;
        this.attributeService = attributeService;
        this.answerService = answerService;
    }

    @Override
    public void run(String... args) {

        int count = assessmentService.findAll().size();

        if (count == 0) {
            loadData();
        }

    }

    private void loadData() {

        // MaturityLevel

        MaturityTrack maturityTrack = new MaturityTrack();
        Set<MaturityTrack> maturityTracks =  new HashSet<>();
        maturityTracks.add(maturityTrack);


        new MaturityLevel(0L, "Puddle Jumper","This is the perfect time to begin " +
                "discussing how consumer needs and mindsets, and new technologies are enabling businesses" +
                " to deliver richer experiences.", true, 0,8 ,maturityTracks );




        // QuestionTypes

        QuestionTypes choice = new QuestionTypes();
        choice.setName("CHOICE");
        questionTypesService.save(choice);

        QuestionTypes sort = new QuestionTypes();
        sort.setName("SORT");
        questionTypesService.save(sort);


        // Track

        Track impact = new Track();
        impact.setName("IMPACT");
        impact.setHighLimit(0);
        impact.setLowLimit(5);
        trackService.save(impact);

        Track data = new Track();
        data.setName("DATA");
        data.setHighLimit(0);
        data.setLowLimit(5);
        trackService.save(data);

        Track technology = new Track();
        technology.setName("TECHNOLOGY");
        technology.setHighLimit(0);
        technology.setLowLimit(5);
        trackService.save(technology);

        Track design = new Track();
        design.setName("DESIGN");
        design.setHighLimit(0);
        design.setLowLimit(5);
        trackService.save(design);

        Track process = new Track();
        process.setName("PROCESS");
        process.setHighLimit(0);
        process.setLowLimit(5);
        trackService.save(process);

        Track culture = new Track();
        culture.setName("CULTURE");
        culture.setHighLimit(0);
        culture.setLowLimit(5);
        trackService.save(culture);


        // Attribute

        Attribute growth = new Attribute();
        growth.setName("CRECIMIENTO");
        attributeService.save(growth);

        Attribute vision = new Attribute();
        vision.setName("VISION");
        attributeService.save(vision);

        Attribute challenges = new Attribute();
        challenges.setName("DESAFIOS");
        attributeService.save(challenges);

        Attribute segmentation = new Attribute();
        segmentation.setName("SEGMENTACION");
        attributeService.save(segmentation);

        Attribute transparency = new Attribute();
        transparency.setName("TRANSPARENCY");
        attributeService.save(transparency);

        Attribute measurement = new Attribute();
        measurement.setName("measurement");
        attributeService.save(measurement);

        Attribute architecture = new Attribute();
        architecture.setName("architecture");
        attributeService.save(architecture);

        Attribute timeToValue = new Attribute();
        timeToValue.setName("timeToValue");
        attributeService.save(timeToValue);

        Attribute collaboration = new Attribute();
        collaboration.setName("collaboration");
        attributeService.save(collaboration);

        Attribute omniRelevant = new Attribute();
        omniRelevant.setName("omniRelevant");
        attributeService.save(omniRelevant);

        Attribute prototyping = new Attribute();
        prototyping.setName("prototyping");
        attributeService.save(prototyping);

        Attribute customerCentricity = new Attribute();
        customerCentricity.setName("customerCentricity");
        attributeService.save(customerCentricity);

        Attribute automation = new Attribute();
        automation.setName("automation");
        attributeService.save(automation);

        Attribute agility = new Attribute();
        agility.setName("agility");
        attributeService.save(agility);

        Attribute experimentation = new Attribute();
        experimentation.setName("experimentation");
        attributeService.save(experimentation);

        Attribute feedback = new Attribute();
        feedback.setName("feedback");
        attributeService.save(feedback);

        Attribute leadership = new Attribute();
        leadership.setName("leadership");
        attributeService.save(leadership);

        Attribute crossCollaboration = new Attribute();
        crossCollaboration.setName("crossCollaboration");
        attributeService.save(crossCollaboration);



        // Question 1

        Set<Answer> answers = new HashSet<>();
        // Answers - Question 1

        answers.add(answerService.save(new Answer(0L,"No",1)));


        answers.add(answerService.save(new Answer(0L,"Yes, we have a high level understanding of the impact of digital in our industry but don't have an" +
                " actionable plan in place.",2)));

        answers.add(answerService.save( new Answer(0L,"Future Readiness Assessment Yes, we have a clear understanding of the impact and have mapped the capabilities required to" +
                " become more competitive.",3)));

        answers.add(answerService.save( new Answer(0L,"Yes, our plan is already in motion and we are achieving our growth targets.",4)));

        Question question1 = loadQuestion(choice,
                impact,
                growth,
                "Does your organization have an understanding of how digital will transform the industry " +
                        "and have a plan for becoming more competitive?",
                40,
                2,
                answers);
        // end - question 1

        // question 2

        answers = new HashSet<>();

        // Answers - Question 2
        answers.add(answerService.save( new Answer(0L,"No",1)));

        answers.add(answerService.save(new Answer(0L,"Yes, we have experimental initiatives to evaluate the impact of a transformation program on our organization.",2)));

        answers.add(answerService.save( new Answer(0L,"Yes, we have clearly defined key performance indicators (KPI's) for tracking how our Digital Transformation.",3)));

        answers.add(answerService.save( new Answer(0L,"Yes, we have an ongoing multidisciplinary transformation program.",4)));

        Question question2 = loadQuestion(choice,
                impact,
                vision,
                "Does your business have a clear vision of the future state and are they able to connect their business success to the organization's transformation strategy?",
                40,
                2,
                answers);
        // end question 2

        // question 3

        answers = new HashSet<>();
        // Answers - Question 3
        answers.add(answerService.save( new Answer(0L,"Aligning the leadership of the organization to the commitment to change",1)));

        answers.add(answerService.save(new Answer(0L,"Our organizations's current technology and data is not suited for a transformation.",2)));

        answers.add(answerService.save( new Answer(0L,"Our organization structure/culture does not encourage transformation.",3)));

        answers.add(answerService.save( new Answer(0L,"The capabilities required to deliver on the value proposition",4)));

        Question question3 = loadQuestion(sort,
                impact,
                challenges,

                "What is the main challenge of a digital transformation? Order the answers based on your priorities",
                20,
                2,
                answers);
        // end question 3



        // question 4
        answers = new HashSet<>();

        // Answers - Question 4
        answers.add(answerService.save( new Answer(0L,"We don't leverage segmentation for our products/services.",1)));

        answers.add(answerService.save(new Answer(0L,"We leverage general demographics segmentation.",2)));

        answers.add(answerService.save( new Answer(0L,"We have a set of segments that enable us to target up to a group level.",3)));

        answers.add(answerService.save( new Answer(0L,"We have a unique set of segments that enables us for 1 to 1 marketing.",4)));

        Question question4 = loadQuestion(choice,
                data,
                segmentation,
                "How confident are you that your target audiences are accurate and well-defined?",
                20,
                2,
                answers);
        // end question 4

        // question 5
        answers = new HashSet<>();

        // Answers - Question 5
        answers.add(answerService.save( new Answer(0L,"Our data is divided into silos and/or segmented systems.",1)));

        answers.add(answerService.save(new Answer(0L,"We have a central repository of data, but access is cumbersome.",2)));

        answers.add(answerService.save( new Answer(0L,"We have self-service tools to access a centralized repository of data.",3)));

        answers.add(answerService.save( new Answer(0L,"We understand which data drives value for our business and extensively use data and analytics to guide desicion making.",4)));

        Question question5 = loadQuestion(choice,
                data,
                transparency,
                "In your organization, how is data accessed to deliver on your value proposition?",
                50,
                2,
                answers);
        // end question 5

        // question 6
        answers = new HashSet<>();

        // Answers - Question 6
        answers.add(answerService.save( new Answer(0L,"We don't have a way to effectively measure the impact of our digital initiatives",1)));

        answers.add(answerService.save(new Answer(0L,"We measure certain iniatives on a per project basis, but we dont have an standarized measurement framework",2)));

        answers.add(answerService.save( new Answer(0L,"We follow a standardized measurement process along the entire lifecycle of the initiative.",3)));

        answers.add(answerService.save( new Answer(0L,"Every initiative we fund follows a standardized measurement process along the entire lifecycle of the initiative.",4)));

        Question question6 = loadQuestion(choice,
                data,
                measurement,
                "Which best describes the process for measuring the impact of digital initiatives in your organization? ",
                30,
                2,
                answers);
        // end question 6

        // question 7
        answers = new HashSet<>();

        // Answers - Question 7
        answers.add(answerService.save( new Answer(0L,"No",1)));

        answers.add(answerService.save(new Answer(0L,"We have a hybrid strategy that still leverages old legacy systems.",2)));

        answers.add(answerService.save( new Answer(0L,"We have successfully created an abstraction layer from our legacy system, enabling better time to market.",3)));

        answers.add(answerService.save( new Answer(0L,"We have an agnostic architecture that leverage componentization to assign responsibilities to each system.",4)));

        Question question7 = loadQuestion(choice,
                technology,
                architecture,
                "Does your architecture allow changes to be deployed quickly and continuously to address business requests?",
                40,
                1,
                answers);
        // end question 7


        // question8
        answers = new HashSet<>();

        // Answers - question8
        answers.add(answerService.save( new Answer(0L,"IT isn't able to support business needs at this point.",1)));

        answers.add(answerService.save(new Answer(0L,"IT can support ongoing operations but isn't able to support innovation work.",2)));

        answers.add(answerService.save( new Answer(0L,"IT is able to provide an acceptable level of service both for ongoing and innovation work.",3)));

        answers.add(answerService.save( new Answer(0L,"IT is a key enabler and driver for our sustainable innovations.",4)));

        Question question8 = loadQuestion(choice,
                technology,
                timeToValue,
                "To what degree does IT blend security, reliability, openness, and agility to support both ongoing operations and innovation work?",
                20,
                1,
                answers);
        // end question8

        // question9
        answers = new HashSet<>();

        // Answers - question9
        answers.add(answerService.save( new Answer(0L,"No, our operation is on-premise.",1)));

        answers.add(answerService.save(new Answer(0L,"Partly, we have moved some of our applications to the cloud.",2)));

        answers.add(answerService.save( new Answer(0L,"Yes, most of our applications are based on cloud, enabling us to scale as needs arise.",3)));

        answers.add(answerService.save( new Answer(0L,"Yes, we have a hybrid cloud architecture.",4)));

        Question question9 = loadQuestion(choice,
                technology,
                collaboration,
                "Is moving business capabilities to the cloud part of your growth strategy?",
                20,
                1,
                answers);
        // end question9

        // question10
        answers = new HashSet<>();

        // Answers - question10
        answers.add(answerService.save( new Answer(0L,"No, interactions with consumers is cumbersome.",1)));

        answers.add(answerService.save(new Answer(0L,"Somewhat; interactions aren't consistent per channel.",2)));

        answers.add(answerService.save( new Answer(0L,"All interactions or touchpoints with customers/users are designed in an integrated way.",3)));

        answers.add(answerService.save( new Answer(0L,"Our experiences are centered around customer needs.",4)));

        Question question10 = loadQuestion(choice,
                design,
                omniRelevant,
                "Is your business able to drive engaging customer experiences?",
                40,
                1,
                answers);
        // end question10

        // question11
        answers = new HashSet<>();

        // Answers - question11
        answers.add(answerService.save( new Answer(0L,"We don't have the capability to design personalized experiences.",1)));

        answers.add(answerService.save(new Answer(0L,"We are able to prototype our initiatives to validate potential issues.",2)));

        answers.add(answerService.save( new Answer(0L,"We are able to scale our prototyping to every project. ",3)));

        answers.add(answerService.save( new Answer(0L,"All of our interactions with consumers are designed and prototyped with the user at the center.",4)));

        Question question11 = loadQuestion(choice,
                design,
                prototyping,
                "Which best describes your organization's ability to design personalized and valuable experiences for the client?",
                20,
                1,
                answers);
        // end question11

        // question12
        answers = new HashSet<>();

        // Answers - question12
        answers.add(answerService.save( new Answer(0L,"Strongly Disagree",1)));

        answers.add(answerService.save(new Answer(0L,"Somewhat Disagree",2)));

        answers.add(answerService.save( new Answer(0L,"Somewhat Agree",3)));

        answers.add(answerService.save( new Answer(0L,"Strongly Agree",4)));

        Question question12 = loadQuestion(choice,
                design,
                customerCentricity,
                "Do you agree with the following statement? \"In my organization, we have a consolidated vision of the user experience.\"",
                40,
                1,
                answers);
        // end question12

        // question13
        answers = new HashSet<>();

        // Answers - question13
        answers.add(answerService.save( new Answer(0L,"No, we dont have self-service tools.",1)));

        answers.add(answerService.save(new Answer(0L,"Partly, we have a degree of automation of internal processes.",2)));

        answers.add(answerService.save( new Answer(0L,"Partly, we have a degree of automation of external processes.",3)));

        answers.add(answerService.save( new Answer(0L,"Every process in the organization is automated and has a self-service layer.",4)));

        Question question13 = loadQuestion(choice,
                process,
                automation,
                "Can your organization, partners and/or customers get the business outcomes required through self-service?",
                40,
                1,
                answers);
        // end question13


        // question14
        answers = new HashSet<>();

        // Answers - question14
        answers.add(answerService.save( new Answer(0L,"No",1)));

        answers.add(answerService.save(new Answer(0L,"Yes, but only for our development teams",2)));

        answers.add(answerService.save( new Answer(0L,"Yes, our product and technology teams are leveraging these methodologies",3)));

        answers.add(answerService.save( new Answer(0L,"Yes, our entire business operates with these methodologies",4)));

        Question question14 = loadQuestion(choice,
                process,
                agility,
                "Does your organization leverage agile methodologies to deliver business value?",
                30,
                1,
                answers);
        // end question14

        // question15
        answers = new HashSet<>();

        // Answers - question15
        answers.add(answerService.save( new Answer(0L,"No, we don't do experimentation.",1)));

        answers.add(answerService.save(new Answer(0L,"Partly, we have separate experiments that dont scale.",2)));

        answers.add(answerService.save( new Answer(0L,"We have successfully scaled certain initiatives.",3)));

        answers.add(answerService.save( new Answer(0L,"All of our initiatives are scaled from an early stage experiment.",4)));

        Question question15 = loadQuestion(choice,
                process,
                agility,
                "Has your company successfully moved a digital initiative from experimentation to scale?",
                30,
                1,
                answers);
        // end question15

        // question16

        answers = new HashSet<>();
        // Answers - question16
        answers.add(answerService.save( new Answer(0L,"Yearly",1)));

        answers.add(answerService.save(new Answer(0L,"Quarterly",2)));

        answers.add(answerService.save( new Answer(0L,"Monthly",3)));

        answers.add(answerService.save( new Answer(0L,"Daily, Anytime",4)));

        Question question16 = loadQuestion(choice,
                culture,
                feedback,
                "How often is feedback given and received by collaborators in your organization?",
                30,
                1,
                answers);
        // end question16

        // question15

        answers = new HashSet<>();
        // Answers - question17
        answers.add(answerService.save( new Answer(0L,"No",1)));

        answers.add(answerService.save(new Answer(0L,"Yes, but we don't have a C-level role to lead our transformation.",2)));

        answers.add(answerService.save( new Answer(0L,"Yes, but our decision-making process is still based on assumptions.",3)));

        answers.add(answerService.save( new Answer(0L,"Yes, we have key C level leadership and the ability to pivot our strategy in short time frames.",4)));

        Question question17 = loadQuestion(choice,
                culture,
                leadership,
                "Do you have the right leadership to drive company-wide engagement and ensure decisions are made fast enough to stay ahead of the market?",
                40,
                1,
                answers);
        // end question17

        // question15

        answers = new HashSet<>();
        // Answers - question18
        answers.add(answerService.save( new Answer(0L,"Each team operates as a silo.",1)));

        answers.add(answerService.save(new Answer(0L,"We have quarterly sessions to align priorities.",2)));

        answers.add(answerService.save( new Answer(0L,"We leverage cells to build multidisciplinary teams.",3)));

        answers.add(answerService.save( new Answer(0L,"The whole company leverages OKRs to understand dependencies.",4)));

        Question question18 = loadQuestion(choice,
                culture,
                crossCollaboration,
                "How do you get buy-in among business, product, engineering and operational leadership?",
                30,
                1,
                answers);
        // end question18


        // Add questions
        Set<Question> questions = new HashSet<>();
        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);
        questions.add(question5);
        questions.add(question6);
        questions.add(question7);
        questions.add(question8);
        questions.add(question9);
        questions.add(question10);
        questions.add(question11);
        questions.add(question12);
        questions.add(question12);
        questions.add(question13);
        questions.add(question14);
        questions.add(question15);
        questions.add(question16);
        questions.add(question17);
        questions.add(question18);

        loadAssessment(questions);


    }

    private Question loadQuestion(QuestionTypes questionType, Track track, Attribute attribute , String message, int weight,int score, Set<Answer> answers) {
        // Question 1

        Question question1 = new Question();
        question1.setTrack(track);
        question1.setAttribute(attribute);
        question1.setName(message);
        question1.setWeight(weight);
        question1.setQuestionTypes(questionType);

        questionService.save(question1);

        question1.setAnswers(answers);
        // Answers Question 1

        questionService.save(question1);

        return question1;
    }

    private void loadAssessment(Set<Question> questions) {
        // Assessment 1
        Assessment assessment = new Assessment();
        assessment.setName("Assessment 1");
        assessment.setLanguage("Es");
        assessment.setQuestions(questions);
        assessmentService.save(assessment);

    }
}
