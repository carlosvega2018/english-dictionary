package com.globant.converge.controllers;

import com.globant.converge.dtos.assessment.*;
import com.globant.converge.services.IAssessmentAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("assessment")
public class AssessmentController {

    @Autowired
    private IAssessmentAttemptService assessmentAttemptService;

    @GetMapping("/{personId}/{language}")
    public ResponseEntity<AssessmentDto> getAssessment(@PathVariable("personId") long personId,
                                                       @PathVariable("language") String language) {
        AssessmentDto assessment = assessmentAttemptService.getAssessment(personId,language);
        return ResponseEntity.status(HttpStatus.OK).body(assessment);
    }

    @PostMapping
    public ResponseEntity<AssessmentResponseDTO> generateAssessment(@RequestBody AssessmentRequestDTO assessmentRequest){
        AssessmentResponseDTO assessmentResponse = assessmentAttemptService.createAssessment( assessmentRequest);
        return ResponseEntity.status(HttpStatus.OK).body(assessmentResponse);
    }

}
