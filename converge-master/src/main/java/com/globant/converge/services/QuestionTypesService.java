package com.globant.converge.services;

import com.globant.converge.model.QuestionTypes;
import com.globant.converge.repositories.QuestionTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionTypesService implements IQuestionTypesService {

    @Autowired
    private QuestionTypesRepository repository;

    public List<QuestionTypes> findAll(){
        return (List<QuestionTypes>) repository.findAll();
    }

    public QuestionTypes findById(Long id){
        return (QuestionTypes) repository.findById(id).get();
    }

    @Override
    public QuestionTypes save(QuestionTypes object) {
        return repository.save(object);
    }

    @Override
    public void delete(QuestionTypes object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);
    }
}
