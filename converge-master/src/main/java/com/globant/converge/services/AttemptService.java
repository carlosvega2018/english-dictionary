package com.globant.converge.services;

import com.globant.converge.model.Attempt;
import com.globant.converge.repositories.AttemptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttemptService implements IAttemptService {

    @Autowired
    private AttemptRepository repository;

    public List<Attempt> findAll(){
        return repository.findAll();
    }

    public Attempt findById(Long id){
        return repository.findById(id).get();
    }

    public Attempt save(Attempt attempt) {
        return repository.save(attempt);
    }
}
