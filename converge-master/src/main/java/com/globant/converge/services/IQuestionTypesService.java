package com.globant.converge.services;

import com.globant.converge.model.QuestionTypes;

public interface IQuestionTypesService extends CrudService<QuestionTypes, Long> {

}
