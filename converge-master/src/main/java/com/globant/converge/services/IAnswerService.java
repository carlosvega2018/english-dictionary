package com.globant.converge.services;

import com.globant.converge.model.Answer;

public interface IAnswerService extends CrudService<Answer, Long>{

}
