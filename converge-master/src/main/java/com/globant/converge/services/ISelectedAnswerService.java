package com.globant.converge.services;

import com.globant.converge.model.SelectedAnswer;

public interface ISelectedAnswerService extends CrudService<SelectedAnswer, Long>  {

}
