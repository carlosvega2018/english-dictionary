package com.globant.converge.services;

import com.globant.converge.model.SelectedAnswer;
import com.globant.converge.repositories.SelectedAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SelectedAnswerService implements ISelectedAnswerService {

    @Autowired
    private SelectedAnswerRepository repository;

    public List<SelectedAnswer> findAll(){
        return repository.findAll();
    }

    public SelectedAnswer findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public SelectedAnswer save(SelectedAnswer object) {
        return repository.save(object);
    }

    @Override
    public void delete(SelectedAnswer object) {
repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {

        repository.deleteById(aLong);
    }
}
