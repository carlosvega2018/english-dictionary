package com.globant.converge.services;

import com.globant.converge.model.Assessment;

public interface IAssessmentService extends CrudService<Assessment,Long> {

    Assessment findByLanguage(String language);
}
