package com.globant.converge.services;

import com.globant.converge.model.Track;

public interface ITrackService extends CrudService<Track, Long>  {

}
