package com.globant.converge.services;

import com.globant.converge.model.AssessmentScore;
import com.globant.converge.repositories.AssessmentScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AssessmentScoreService implements IAssessmentScoreService {

    @Autowired
    private AssessmentScoreRepository repository;

    public List<AssessmentScore> findAll(){
        return repository.findAll();
    }

    public AssessmentScore findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public AssessmentScore save(AssessmentScore object) {
        return repository.save(object);
    }

    @Override
    public void delete(AssessmentScore object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
