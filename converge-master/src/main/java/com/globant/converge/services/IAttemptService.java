package com.globant.converge.services;

import com.globant.converge.model.Attempt;

import java.util.List;

public interface IAttemptService {
    public List<Attempt> findAll();

    public Attempt findById(Long id);

    Attempt save(Attempt attempt);
}
