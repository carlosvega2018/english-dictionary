package com.globant.converge.services;

import com.globant.converge.model.QuestionSolved;
import com.globant.converge.repositories.QuestionSolvedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionSolvedService implements IQuestionSolvedService {

    @Autowired
    private QuestionSolvedRepository repository;

    public List<QuestionSolved> findAll(){
        return repository.findAll();
    }

    public QuestionSolved findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public QuestionSolved save(QuestionSolved object) {
        return repository.save(object);
    }

    @Override
    public void delete(QuestionSolved object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
    repository.deleteById(aLong);
    }
}
