package com.globant.converge.services;

import com.globant.converge.model.Attribute;

public interface IAttributeService extends CrudService<Attribute, Long> {

}
