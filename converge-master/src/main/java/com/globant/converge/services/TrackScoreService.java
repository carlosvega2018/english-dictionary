package com.globant.converge.services;

import com.globant.converge.model.TrackScore;
import com.globant.converge.repositories.TrackScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackScoreService implements ITrackScoreService {

    @Autowired
    private TrackScoreRepository repository;

    public List<TrackScore> findAll(){
        return repository.findAll();
    }

    public TrackScore findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public TrackScore save(TrackScore object) {
        return repository.save(object);
    }

    @Override
    public void delete(TrackScore object) {
        repository.delete(object);

    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
