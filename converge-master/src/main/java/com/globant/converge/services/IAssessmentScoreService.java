package com.globant.converge.services;

import com.globant.converge.model.AssessmentScore;

public interface IAssessmentScoreService extends CrudService<AssessmentScore, Long>{
}
