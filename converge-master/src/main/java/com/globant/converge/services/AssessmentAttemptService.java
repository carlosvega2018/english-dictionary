package com.globant.converge.services;

import com.globant.converge.dtos.assessment.*;
import com.globant.converge.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AssessmentAttemptService implements IAssessmentAttemptService{

    @Autowired
    private IAssessmentService assessmentService;

    @Autowired
    private IAttemptService attemptService;

    @Autowired
    private IPersonService personService;


    @Autowired
    private IQuestionService questionService;

    @Autowired
    private IQuestionSolvedService questionSolvedService;

    @Autowired
    private IAnswerService answerService;

    @Autowired
    private IMaturityLevelService maturityLevelService;

    @Autowired
    private ISelectedAnswerService selectedAnswerService;

    @Autowired
    private ITrackScoreService trackScoreService;

    @Autowired
    private ITrackService trackService;


    public AssessmentDto getAssessment(long attemptId, String language) {
        Assessment assessment = assessmentService.findByLanguage(language);
        Attempt attempt = attemptService.findById(attemptId);
        if (attempt != null){
            attempt.setAssessment(assessment);
            this.attemptService.save(attempt);
        }
       return  assessmentToAssessmentDTO(assessment);

    }

    private AssessmentDto assessmentToAssessmentDTO(Assessment assessment) {
        AssessmentDto dto = new AssessmentDto();
        ModelMapper mapper = new ModelMapper();
        if(assessment!=null){
            dto.setId(assessment.getId());
            List<QuestionDto> questionDtos = new ArrayList<>();
            for (Question question:assessment.getQuestions()) {
                QuestionDto questionDto = new QuestionDto();
                questionDto.setId(question.getId());
                questionDto.setName(question.getName());
                List<AnswerDto> answerDtos = new ArrayList<>();
                for (Answer answer:question.getAnswers()) {
                    answerDtos.add(mapper.map(answer,AnswerDto.class));
                }
                questionDto.setAnswers(answerDtos);
                questionDtos.add(questionDto);
            }
            dto.setQuestions(questionDtos);
        }
        return dto;
    }


    // calc scoring :  find scoring ,  sum scoring
    // create a method to calculate the maturity
    // insert into question solved
    // insert into selected answer
    // insert into trackscore
    // update attempt
    public AssessmentResponseDTO createAssessment(AssessmentRequestDTO assessmentRequest) {
        Assessment assessment = assessmentService.findById( assessmentRequest.getAssessmentId() );

        List<AnswersCombined> answers =  assessmentRequest.getAnswersCombined();

        Attempt attempt = attemptService.findById(assessmentRequest.getAttemptId());

        //create maps de tracks para guardar en track_score
        int score = 0;

        Map<Long, Integer> trackScoreMap = new HashMap<>();
        for ( AnswersCombined answersCombined  : answers  ) {
            int scoreQuestion = 0;

            Question question = questionService.findById(answersCombined.getQuestionId());
            Answer answerFetched = answerService.findById(answersCombined.getAnswerId());

            scoreQuestion = (question.getWeight() * answerFetched.getBias());

            trackScoreMap.put(answersCombined.getQuestionTrackId(),  trackScoreMap.get(answersCombined.getQuestionTrackId())  + scoreQuestion );

            score += scoreQuestion;


            QuestionSolved questionSolved = new QuestionSolved();
            questionSolved.setAttempt(attempt );
            questionSolved.setQuestion(question);
            questionSolved.setName(answerFetched.getName());
            questionSolvedService.save(questionSolved);

            SelectedAnswer selectedAnswer = new SelectedAnswer();
            selectedAnswer.setAnswerId(answersCombined.getAnswerId());
            //     selectedAnswer.setQuestionSolved(questionSolved);
            selectedAnswer.setOrderAnswer(answersCombined.getAnswerOrder());
            selectedAnswerService.save(selectedAnswer);


        }

        for ( Map.Entry<Long, Integer> entry : trackScoreMap.entrySet()   ) {
            TrackScore trackScore = new TrackScore();
            trackScore.setAttempt(attempt);
            trackScore.setScore(entry.getValue());
            Track track = trackService.findById(entry.getKey());
            trackScore.setTrack(track);
            trackScoreService.save(trackScore);
        }

        attemptService.save(attempt);

        AssessmentResponseDTO assessmentResponse = new AssessmentResponseDTO();

        //Maturity / Search it
        String maturityName = defineCategory(score);
        MaturityLevelDTO maturityLevelDTO = new MaturityLevelDTO();
        maturityLevelDTO.setId(1);;
        maturityLevelDTO.setName("Maturiryname");

        //Tracks and itsgrad
        List<TrackDTO> trackListDTO = new ArrayList<>();
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.setId(1);
        trackDTO.setName("test trackdto");
        trackDTO.setGrade(2.5);

        trackListDTO.add(trackDTO);

        assessmentResponse.setGrade(score);
        assessmentResponse.setMaturityLevelDTO(maturityLevelDTO);
        assessmentResponse.setTrackListDTO(trackListDTO);

        return assessmentResponse;
    }


    public String defineCategory (float score){
        if (score > 5) return "Rain Maker";
        if (score > 4) return "Weather Cultivator";
        if (score > 3) return "Storm Forecaster";
        if (score > 2) return "Cloud Chaser";
        if (score > 1) return "Wind Breaker";
        return "Puddle Jumper";
    }


}