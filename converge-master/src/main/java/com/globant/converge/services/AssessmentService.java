package com.globant.converge.services;

import com.globant.converge.model.Assessment;
import com.globant.converge.repositories.AssessmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssessmentService implements IAssessmentService {

    @Autowired
    private AssessmentRepository repository;

    public List<Assessment> findAll(){
        return (List<Assessment>) repository.findAll();
    }

    public Assessment findById(Long id){
        return (Assessment) repository.findById(id).get();
    }

    public Assessment findByLanguage(String language) {
        return repository.findByLanguage(language);
    }

    @Override
    public Assessment save(Assessment object) {
        return repository.save(object);
    }

    @Override
    public void delete(Assessment object) {
    repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
    deleteById(aLong);
    }
}
