package com.globant.converge.services;

import com.globant.converge.model.AttemptStatus;
import com.globant.converge.repositories.AttemptStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AttemptStatusService implements IAttemptStatusService {

    @Autowired
    private AttemptStatusRepository repository;

    public List<AttemptStatus> findAll(){
        return repository.findAll();
    }

    public AttemptStatus findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public AttemptStatus save(AttemptStatus object) {
        return repository.save(object);
    }

    @Override
    public void delete(AttemptStatus object) {
        repository.delete(object);

    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
