package com.globant.converge.services;

import com.globant.converge.model.Person;

public interface IPersonService extends CrudService<Person, Long>  {

    // pending change to save
    long register(Person person);

    Person findByEmail(String email);
}
