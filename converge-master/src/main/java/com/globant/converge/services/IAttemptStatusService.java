package com.globant.converge.services;

import com.globant.converge.model.AttemptStatus;

public interface IAttemptStatusService extends CrudService<AttemptStatus, Long>{

}
