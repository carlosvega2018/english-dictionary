package com.globant.converge.services;

import com.globant.converge.model.QuestionSolved;

public interface IQuestionSolvedService  extends CrudService<QuestionSolved, Long> {

}
