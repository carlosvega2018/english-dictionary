package com.globant.converge.services;

import com.globant.converge.model.TrackScore;

public interface ITrackScoreService  extends CrudService<TrackScore, Long> {

}
