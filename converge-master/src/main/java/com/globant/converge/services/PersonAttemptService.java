package com.globant.converge.services;

import com.globant.converge.model.Attempt;
import com.globant.converge.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonAttemptService implements IPersonAttemptService{

    @Autowired
    private IAttemptService attemptService;

    @Autowired
    private IPersonService personService;

    public long createAttempt(Person person){
        Person registeredPerson = personService.findByEmail(person.getEmail());
        if (registeredPerson == null) {
            registeredPerson = personService.save(person);
        }

        Attempt attempt = new Attempt();
        attempt.setAttemptStatus(null);
        attempt.setMaturityLevel(null);
        attempt.setAssessment(null);
        attempt.setPerson(person);

        attempt = attemptService.save(attempt);

        return attempt.getId();
    }
}
