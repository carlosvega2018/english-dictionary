package com.globant.converge.services;

import com.globant.converge.model.MaturityLevel;
import com.globant.converge.repositories.MaturityLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaturityLevelService implements IMaturityLevelService {

    @Autowired
    private MaturityLevelRepository repository;

    public List<MaturityLevel> findAll(){
        return repository.findAll();
    }

    public MaturityLevel findById(Long id){
        return repository.findById(id).get();
    }

    @Override
    public MaturityLevel save(MaturityLevel object) {
        return repository.save(object);
    }

    @Override
    public void delete(MaturityLevel object) {
        repository.delete(object);

    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
