package com.globant.converge.services;

import com.globant.converge.dtos.assessment.AssessmentDto;
import com.globant.converge.dtos.assessment.AssessmentRequestDTO;
import com.globant.converge.dtos.assessment.AssessmentResponseDTO;

public interface IAssessmentAttemptService {

    AssessmentDto getAssessment(long attemptId, String language);

    AssessmentResponseDTO createAssessment(AssessmentRequestDTO assessmentRequest);

}
