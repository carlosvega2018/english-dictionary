package com.globant.converge.services;

import com.globant.converge.model.Person;

public interface IPersonAttemptService {

    long createAttempt(Person person);
}
