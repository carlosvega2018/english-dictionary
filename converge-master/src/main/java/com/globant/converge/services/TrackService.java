package com.globant.converge.services;

import com.globant.converge.model.Track;
import com.globant.converge.repositories.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TrackService implements ITrackService {

    @Autowired
    private TrackRepository repository;

    public List<Track> findAll() {
        return (List<Track>) repository.findAll();
    }

    public Track findById(Long id) {
        return (Track) repository.findById(id).get();
    }

    @Override
    public Track save(Track object) {
        return repository.save(object);
    }

    @Override
    public void delete(Track object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
