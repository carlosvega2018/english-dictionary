package com.globant.converge.services;

import com.globant.converge.model.Person;
import com.globant.converge.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService implements IPersonService {

    @Autowired
    private PersonRepository repository;

    public List<Person> findAll() {
        return repository.findAll();
    }

    public Person findById(Long id) {

        Optional<Person> personOptional = repository.findById(id);
        return personOptional.isPresent() ? personOptional.get() : null;
    }


    public long register(Person person) {
        Person registeredPerson = repository.findByEmail(person.getEmail());
        if (registeredPerson == null) {
            registeredPerson = repository.save(person);
        }
        return registeredPerson.getId();
    }

    @Override
    public Person save(Person object) {
        return repository.save(object);
    }

    @Override
    public void delete(Person object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);
    }

    public Person findByEmail(String email){
        return repository.findByEmail(email);
    }
}
