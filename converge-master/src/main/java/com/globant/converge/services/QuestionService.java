package com.globant.converge.services;

import com.globant.converge.model.Question;
import com.globant.converge.repositories.QuestionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService implements IQuestionService {

    private final QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Question> findAll() {
        return null;
    }

    @Override
    public Question findById(Long id) {
        return null;
    }

    @Override
    public Question save(Question object) {
        return questionRepository.save(object);
    }

    @Override
    public void delete(Question object) {
        questionRepository.delete(object);

    }

    @Override
    public void deleteById(Long aLong) {
        questionRepository.deleteById(aLong);
    }
}
