package com.globant.converge.services;

import com.globant.converge.model.Question;

public interface IQuestionService extends CrudService<Question, Long>  {

}
