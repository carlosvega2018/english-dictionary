package com.globant.converge.services;

import com.globant.converge.model.Answer;
import com.globant.converge.repositories.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerService implements IAnswerService {

    @Autowired
    private AnswerRepository repository;

    public List<Answer> findAll(){
        return (List<Answer>) repository.findAll();
    }

    public Answer findById(Long id){
        return (Answer) repository.findById(id).get();
    }


    @Override
    public Answer save(Answer object) {
        return repository.save(object);
    }

    @Override
    public void delete(Answer object) {
        repository.delete(object);

    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);
    }

}
