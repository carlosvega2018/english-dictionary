package com.globant.converge.services;

import com.globant.converge.model.Attribute;
import com.globant.converge.repositories.AttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttributeService implements IAttributeService {

    @Autowired
    private AttributeRepository repository;

    public List<Attribute> findAll() {
        return (List<Attribute>) repository.findAll();
    }

    public Attribute findById(Long id) {
        return (Attribute) repository.findById(id).get();
    }

    @Override
    public Attribute save(Attribute object) {
        return repository.save(object);
    }

    @Override
    public void delete(Attribute object) {
        repository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        repository.deleteById(aLong);

    }
}
