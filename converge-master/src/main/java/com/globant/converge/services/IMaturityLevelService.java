package com.globant.converge.services;

import com.globant.converge.model.MaturityLevel;

public interface IMaturityLevelService extends CrudService<MaturityLevel, Long> {

}
