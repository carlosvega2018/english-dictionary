package com.globant.converge.repositories;

import com.globant.converge.model.AttemptStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttemptStatusRepository extends JpaRepository<AttemptStatus,Long> {
}
