package com.globant.converge.repositories;

import com.globant.converge.model.QuestionTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionTypesRepository extends JpaRepository<QuestionTypes, Long> {

}
