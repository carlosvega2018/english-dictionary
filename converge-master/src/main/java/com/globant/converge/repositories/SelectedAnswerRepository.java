package com.globant.converge.repositories;

import com.globant.converge.model.SelectedAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SelectedAnswerRepository extends JpaRepository<SelectedAnswer, Long> {
}
