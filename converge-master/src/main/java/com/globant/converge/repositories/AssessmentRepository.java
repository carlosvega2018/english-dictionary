package com.globant.converge.repositories;

import com.globant.converge.model.Assessment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssessmentRepository extends JpaRepository<Assessment, Long> {

    Assessment findByLanguage(String language);
}
