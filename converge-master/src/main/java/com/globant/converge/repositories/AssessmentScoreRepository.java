package com.globant.converge.repositories;

import com.globant.converge.model.AssessmentScore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssessmentScoreRepository extends JpaRepository<AssessmentScore, Long> {
}
