package com.globant.converge.repositories;

import com.globant.converge.model.Attempt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttemptRepository extends JpaRepository<Attempt,Long> {
}
