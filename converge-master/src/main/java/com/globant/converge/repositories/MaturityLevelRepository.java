package com.globant.converge.repositories;

import com.globant.converge.model.MaturityLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaturityLevelRepository extends JpaRepository<MaturityLevel,Long> {
}
