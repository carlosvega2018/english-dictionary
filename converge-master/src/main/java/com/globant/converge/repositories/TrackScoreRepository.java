package com.globant.converge.repositories;

import com.globant.converge.model.TrackScore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackScoreRepository extends JpaRepository<TrackScore, Long> {
}
