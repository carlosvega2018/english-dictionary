package com.globant.converge.repositories;

import com.globant.converge.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Long> {

    Person findByEmail(String email);
}
