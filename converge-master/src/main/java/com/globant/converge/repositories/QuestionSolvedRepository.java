package com.globant.converge.repositories;

import com.globant.converge.model.QuestionSolved;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionSolvedRepository extends JpaRepository<QuestionSolved, Long> {
}
