package com.globant.converge.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", length = 1000)
    private String name;

    @Column(name="weight")
    private int weight;

    //TODO Revisar si podemos manejarlo como ID
    @ManyToOne
    @JoinColumn (name="question_types_id")
    private QuestionTypes questionTypes;

    @ManyToOne
    @JoinColumn (name="track_id")
    private Track track;

//    @ManyToMany(mappedBy = "questions")
//    private Set<Assessment> assessment;

    //TODO Revisar relacion
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="question_id")
    private Set<Answer> answers;

    @ManyToOne
    @JoinColumn (name="attribute_id")
    private Attribute attribute;


}
