package com.globant.converge.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "tracks")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", length = 255)
    private String name;

    @Column(name="low_limit")
    private int lowLimit;

    @Column(name="high_limit")
    private int highLimit;

    @JsonIgnore
    @ManyToOne
    @JoinColumn (name="assessment_id")
    private Assessment assessment;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "track")
    private Set<MaturityTrack> maturityTracks;





}
