package com.globant.converge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "answers")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 1000)
    private String name;

    //private long idQuestion;
//    @ManyToOne
//    @JoinColumn(name="question_id")
//    @JsonIgnore
//    private Question question;

    @Column(name="bias")
    private int bias;

}
