package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "selected_answer")
public class SelectedAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "order_answer")
    private int orderAnswer;

    @Column(length = 1000)
    private String name;

    @Column(name = "answer_id")
    private long answerId;
}
