package com.globant.converge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "maturity_levels")
public class MaturityLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    @Column(length = 1000)
    private String description;
    private boolean status;
    private int lowLimit;
    private int highLimit;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "maturityLevel")
    private Set<MaturityTrack> maturityTracks;
}
