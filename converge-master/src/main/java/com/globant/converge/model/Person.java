package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "persons")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Basic
    private String email;
    private String firstName;
    private String company;
    private String lastName;
    private String mobilePhone;

    @OneToMany(mappedBy = "person")
    private List<Attempt> attempts;
}
