package com.globant.converge.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "assessments")
public class Assessment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="language", length = 2)
    private String language;

    @ManyToMany
    @JoinTable(name = "assessment_questions",
                joinColumns = @JoinColumn(name = "assessment_id"),
                 inverseJoinColumns =  @JoinColumn(name="questions_id"))
    private Set<Question> questions;


    /*public String toString(){

        for (int i=0;i<this.questions.size();i++){
            this.questions.
        }

        for (Question q:this.questions) {
            System.out.println(q.getName());
        }
    }*/

}
