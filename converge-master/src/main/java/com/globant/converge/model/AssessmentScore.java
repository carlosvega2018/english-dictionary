package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "assessment_scores")
public class AssessmentScore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(name="category_name")
    String categoryName;
    @Column(name="high_limit")
    int highLimit;
    @Column(name="low_limit")
    int lowLimit;

    @ManyToOne
    @JoinColumn(name = "id_assessment")
    Assessment assessment;



}
