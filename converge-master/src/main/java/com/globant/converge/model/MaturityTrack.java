package com.globant.converge.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "maturity_tracks")
public class MaturityTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Lob
    private String description;

    @ManyToOne
    private MaturityLevel maturityLevel;

    @ManyToOne
    private Track track;
}
