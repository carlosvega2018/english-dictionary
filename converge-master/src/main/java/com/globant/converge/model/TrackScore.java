package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "track_scores")
public class TrackScore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(name = "category_name")
    String categoryName;

    int score;

    @ManyToOne
    @JoinColumn(name = "id_track")
    Track track;

    @ManyToOne
    @JoinColumn(name = "id_attempt")
    Attempt attempt;


}
