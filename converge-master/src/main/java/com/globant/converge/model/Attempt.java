package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "attempts")
public class Attempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @ManyToOne
    @JoinColumn(name = "archetype_id")
    private Archetype archetype;

    @ManyToOne
    @JoinColumn(name = "maturity_level_id")
    private MaturityLevel maturityLevel;

    @ManyToOne
    @JoinColumn(name = "attempt_status_id")
    private AttemptStatus attemptStatus;

    @ManyToOne
    @JoinColumn(name = "assessment_id")
    private Assessment assessment;

}
