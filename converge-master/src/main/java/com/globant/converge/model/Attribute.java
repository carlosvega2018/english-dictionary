package com.globant.converge.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "attributes")
public class Attribute {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", length = 255)
    private String name;
}
