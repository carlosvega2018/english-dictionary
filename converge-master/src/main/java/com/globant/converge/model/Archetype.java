package com.globant.converge.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "archetypes")
public class Archetype {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int language;
    private String code;
    private String description;
    private String image;
}
