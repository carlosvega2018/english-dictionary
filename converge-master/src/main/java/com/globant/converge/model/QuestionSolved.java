package com.globant.converge.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "question_solved")
public class QuestionSolved {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    @Column(length = 1000)
    String name;

    @Column(name="score")
    private int score;

   @OneToMany(cascade = CascadeType.ALL)
   @JoinColumn(name = "question_solved_id")
    private Set<SelectedAnswer> selectedAnswers;

    @ManyToOne
    @JoinColumn(name = "id_attempt")
    Attempt attempt;

    @ManyToOne
    @JoinColumn(name = "id_question")
    Question question;

}
