package dictionary.dictionaryapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WordRepo extends JpaRepository {
}
