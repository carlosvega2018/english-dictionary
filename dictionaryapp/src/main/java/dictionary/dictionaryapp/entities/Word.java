package dictionary.dictionaryapp.entities;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Word {


    private String name;
    private String meaning;

    public Word(){

    }

    public Word (String name, String meaning){
        this.name = name;
        this.meaning = meaning;
    }

}
