package dictionary.dictionaryapp.services.impl;


import dictionary.dictionaryapp.entities.Word;
import dictionary.dictionaryapp.repository.WordRepo;
import dictionary.dictionaryapp.services.exceptions.WordServiceException;
import dictionary.dictionaryapp.services.interfaces.IWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WordServiceImpl implements IWordService {


    @Autowired
    WordRepo wordRepo;

    @Override
    public List<Word> findAll() throws WordServiceException{
        return this.wordRepo.findAll();
    }

    @Override
    public void save(Word word) throws WordServiceException{
        try {
            Object save = this.wordRepo.save(word);
        }catch (Exception e){
            throw new WordServiceException(e,02,"Error creating");
        }
    }

    @Override
    public void delete(Long id) throws WordServiceException{
        try{
            this.wordRepo.deleteById(id);
        }catch(Exception e){
            throw new WordServiceException(e,03,"Error deleting");
        }
    }

    @Override
    public Word findById(Long id) throws WordServiceException{
        return null;
    }

}
