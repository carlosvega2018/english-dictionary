package dictionary.dictionaryapp.services.exceptions;


import lombok.Getter;

@Getter
public class WordServiceException extends RuntimeException {

    private Integer errorCode;
    private String description;


    public WordServiceException(Integer errorCode,
                                String description){
        this.errorCode = errorCode;
        this.description = description;
    }
    public WordServiceException(Exception cause,
                                Integer errorCode,
                                String description){
        super(cause);
        this.errorCode = errorCode;
        this.description = description;
    }



}
