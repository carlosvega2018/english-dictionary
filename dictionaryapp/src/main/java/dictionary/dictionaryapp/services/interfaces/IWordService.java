package dictionary.dictionaryapp.services.interfaces;

import dictionary.dictionaryapp.entities.Word;
import dictionary.dictionaryapp.services.exceptions.WordServiceException;

import java.util.List;

public interface IWordService {

    List<Word> findAll() throws WordServiceException;
    void save(Word word) throws WordServiceException;
    void delete(Long id) throws WordServiceException;
    Word findById(Long id) throws WordServiceException;


}
