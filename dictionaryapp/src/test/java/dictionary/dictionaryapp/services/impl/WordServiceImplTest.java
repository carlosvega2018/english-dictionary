package dictionary.dictionaryapp.services.impl;

import dictionary.dictionaryapp.entities.Word;
import dictionary.dictionaryapp.repository.WordRepo;
import dictionary.dictionaryapp.services.exceptions.WordServiceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class WordServiceImplTest {

    @InjectMocks
    WordServiceImpl wordService;

    @Mock
    WordRepo wordRepo;

    @Test
    public void findAllTest1() {
        List<Word> words = new ArrayList<Word>();

        words.add(new Word("Play", "Play meaning"));
        words.add(new Word("Eat", "Eat meaning"));

        when(wordRepo.findAll()).thenReturn(words);

        try {
            Assert.assertEquals(words.size(),
                                wordService.findAll().size());
        }catch(WordServiceException e) {
            Assert.fail(e.getDescription());
        }
    }

    @Test
    public void saveTestTry(){

        Word word = new Word("enjoy","enjoy meaning");

        when(this.wordRepo.save(word))
                .thenReturn(new Word());

        try {
            this.wordService.save(word);
            Assert.assertEquals(true,
                                true);
        }catch (WordServiceException e){
            Assert.fail(e.getDescription());
        }
    }

    @Test(expected = WordServiceException.class)
    public void saveTestCatch(){
        Word word = new Word("fail","Fail definition");

        doThrow(new WordServiceException(10,"throwing exception"))
            .when(this.wordRepo).save(word);
        this.wordService.save(word);
    }

    @Test
    public void deleteTestTry() {
        Long id = 1L;

        doNothing().when(this.wordRepo).deleteById(id);

        try {
            this.wordService.delete(id);
            Assert.assertTrue(true);
        } catch (WordServiceException e){
            Assert.assertFalse(true);
        }
    }

}